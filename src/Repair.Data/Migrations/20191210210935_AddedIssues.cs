﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Repair.Data.Migrations
{
    public partial class AddedIssues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Issues",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    ModelId = table.Column<string>(nullable: true),
                    ModelId1 = table.Column<long>(nullable: true),
                    ProductionYear = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Vin = table.Column<string>(nullable: true),
                    ClientId = table.Column<long>(nullable: false),
                    DesiredPrice = table.Column<decimal>(nullable: false),
                    SelectedOfferId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Issues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Issues_CarModels_ModelId1",
                        column: x => x.ModelId1,
                        principalTable: "CarModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Issues_ModelId1",
                table: "Issues",
                column: "ModelId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Issues");
        }
    }
}
