﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Repair.Data.Migrations
{
    public partial class IssueKeyFixed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Issues_CarModels_ModelId1",
                table: "Issues");

            migrationBuilder.DropIndex(
                name: "IX_Issues_ModelId1",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "ModelId1",
                table: "Issues");

            migrationBuilder.AlterColumn<long>(
                name: "ModelId",
                table: "Issues",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Issues_ModelId",
                table: "Issues",
                column: "ModelId");

            migrationBuilder.AddForeignKey(
                name: "FK_Issues_CarModels_ModelId",
                table: "Issues",
                column: "ModelId",
                principalTable: "CarModels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Issues_CarModels_ModelId",
                table: "Issues");

            migrationBuilder.DropIndex(
                name: "IX_Issues_ModelId",
                table: "Issues");

            migrationBuilder.AlterColumn<string>(
                name: "ModelId",
                table: "Issues",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<long>(
                name: "ModelId1",
                table: "Issues",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Issues_ModelId1",
                table: "Issues",
                column: "ModelId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Issues_CarModels_ModelId1",
                table: "Issues",
                column: "ModelId1",
                principalTable: "CarModels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
