﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repair.Data.Entities
{
    public class Issue : EntityBase
    {
        /// <summary>
        /// Car model
        /// </summary>
        public long ModelId { get; set; }
        public CarModel Model { get; set; }

        /// <summary>
        /// The year car has been produced
        /// </summary>
        public int ProductionYear { get; set; }
        /// <summary>
        /// Issue description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Car's unique identification number
        /// </summary>
        public string Vin { get; set; }
        /// <summary>
        /// Client's unique identity
        /// </summary>
        public long ClientId { get; set; }

        /// <summary>
        /// Rough price, specified by the client
        /// </summary>
        public decimal DesiredPrice { get; set; }
        /// <summary>
        /// Offer, that client selected. Can be null
        /// </summary>
        public long? SelectedOfferId { get; set; }
        
        public ICollection<Offer> Offers { get; set; }
    }
}
