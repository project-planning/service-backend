﻿using Repair.Data.Enums;
using System;

namespace Repair.Data.Entities
{
    public class UserRole : EntityBase
    {
        public long UserId { get; set; }

        public User User { get; set; }

        public Role Role { get; set; }
    }
}
