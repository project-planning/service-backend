﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repair.Data.Entities
{
    public class Offer
    {
        public long Id { get; set; }
        
        public long IssueId { get; set; }
        
        public Issue Issue { get; set; }
        
        public decimal Price { get; set; }
        
        public string Description { get; set; }
        
        public long AutoServiceId { get; set; }
        
        public AutoService AutoService { get; set; }
    }
}
