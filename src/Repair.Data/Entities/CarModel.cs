﻿using Repair.Data.Enums;
using System.Collections.Generic;

namespace Repair.Data.Entities
{
    public class CarModel
    {
        public long Id { get; set; }

        public long ManufacturerId { get; set; }

        public Manufacturer Manufacturer { get; set; }

        public string ModelName { get; set; }

        public string AdditionalInfo { get; set; }

        public int DoorCount { get; set; }

        public int ProductionStartYear { get; set; }

        public int? ProductionEndYear { get; set; }

        public TransmissionType TransmissionType { get; set; }

        public FuelType FuelType { get; set; }

        public string PictureUrl { get; set; }


        public ICollection<Issue> Issues { get; set; }
    }
}
