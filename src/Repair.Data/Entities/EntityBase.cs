﻿using System;

namespace Repair.Data.Entities
{
    public class EntityBase
    {
        public long Id { get; set; }

        public DateTime Created { get; set; } = DateTime.UtcNow;
    }
}
