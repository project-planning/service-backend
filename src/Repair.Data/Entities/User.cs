﻿using Repair.Data.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Repair.Data.Entities
{
    public class User : EntityBase
    {
        [StringLength(100)]
        [Required]
        public string UserName { get; set; }

        [Required, StringLength(128)]
        public string Email { get; set; }

        [StringLength(255)]
        public string? PasswordHash { get; set; }

        public ICollection<UserRole> Roles { get; set; }
    }
}
