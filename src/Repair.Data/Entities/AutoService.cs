namespace Repair.Data.Entities
{
    public class AutoService
    {
        public long Id { get; set; }
        
        public long UserId { get; set; }
        
        public User User { get; set; }
        
        public string Name { get; set; }
        
        public string Description { get; set; }
        
        public string Address { get; set; }
        
        public string ContactEmail { get; set; }
        
        public string ContactPhone { get; set; }
    }
}