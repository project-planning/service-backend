﻿using System.Collections.Generic;

namespace Repair.Data.Entities
{
    public class Manufacturer
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public string Country { get; set; }

        public ICollection<CarModel> Models { get; set; }
    }
}
