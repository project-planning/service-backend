﻿namespace Repair.Data.Enums
{
    public enum FuelType
    {
        Gasoline,
        Gas,
        Electric,
        Diesel
    }
}
