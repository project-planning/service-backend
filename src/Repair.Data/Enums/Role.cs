﻿namespace Repair.Data.Enums
{
    public enum Role
    {
        RegularUser,
        RepairShop,
        Moderator,
        Admin
    }
}
