﻿namespace Repair.Data.Enums
{
    public enum TransmissionType
    {
        Other,
        Automatic,
        Manual,
        AutomatedManual,
        ContinuouslyVariable
    }
}
