﻿using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Repair.Data.Entities;
using Repair.Data.Enums;

namespace Repair.Data
{
    public class AutoserviceContext : DbContext, IDataProtectionKeyContext
    {
        public AutoserviceContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserRole>()
                .Property(role => role.Role)
                .HasConversion(new EnumToStringConverter<Role>());

            DataSeeder.FillInitialValues(modelBuilder);

            modelBuilder.Entity<CarModel>()
                .Property(model => model.TransmissionType)
                .HasConversion(new EnumToStringConverter<TransmissionType>());

            modelBuilder.Entity<CarModel>()
                .Property(model => model.FuelType)
                .HasConversion(new EnumToStringConverter<FuelType>());

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<User> Users { get; set; }

        public DbSet<UserRole> UserRoles { get; set; }

        public DbSet<DataProtectionKey> DataProtectionKeys { get; set; }

        public DbSet<Manufacturer> Manufacturers { get; set; }

        public DbSet<CarModel> CarModels { get; set; }

        public DbSet<Issue> Issues { get; set; }
        
        public DbSet<Offer> Offers { get; set; }
        
        public DbSet<AutoService> AutoServices { get; set; }
    }
}
