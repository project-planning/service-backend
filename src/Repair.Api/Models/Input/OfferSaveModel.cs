namespace Repair.Api.Models.Input
{
    public class OfferSaveModel
    {
        public long IssueId { get; set; }
        
        public decimal Price { get; set; }
        
        public string Description { get; set; }
    }
}