﻿using Swashbuckle.AspNetCore.Filters;
using System.ComponentModel.DataAnnotations;
using FluentValidation;

namespace Repair.Api.Models.Input
{
    public class RegisterUserModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        internal class Example : IExamplesProvider<RegisterUserModel>
        {
            public RegisterUserModel GetExamples()
            {
                return new RegisterUserModel
                {
                    Email = "test@test.org",
                    UserName = "test",
                    Password = "123456"
                };
            }
        }
        
        internal class RegisterModelValidator : AbstractValidator<RegisterUserModel>
        {
            public RegisterModelValidator()
            {
                RuleFor(model => model.Email).EmailAddress();
                RuleFor(model => model.Password).MinimumLength(2);
                RuleFor(model => model.UserName).MaximumLength(100);
            }
        }
    }
}
