using System.ComponentModel.DataAnnotations;
using Swashbuckle.AspNetCore.Filters;

namespace Repair.Api.Models.Input
{
    public class AuthenticateModel
    {
        [Required]
        public string Login { get; set; }
        
        [Required]
        public string Password { get; set; }

        internal class Example : IExamplesProvider<AuthenticateModel>
        {
            public AuthenticateModel GetExamples()
            {
                return new AuthenticateModel
                {
                    Login = "test",
                    Password = "123456"
                };
            }
        }
    }
    
    
}