﻿using System.ComponentModel.DataAnnotations;

namespace Repair.Api.Models.Input
{
    public class IssueSaveModel
    {
        /// <summary>
        /// Car manufacturer company
        /// </summary>
        [Required]
        public long ManufacturerId { get; set; }
        /// <summary>
        /// Car model
        /// </summary>
        [Required]
        public long ModelId { get; set; }
        /// <summary>
        /// The year car has been produced
        /// </summary>
        [Required]
        public int ProductionYear { get; set; }
        /// <summary>
        /// Issue description
        /// </summary>
        [Required]
        public string Description { get; set; }
        /// <summary>
        /// Car's unique identification number
        /// </summary>
        [Required]
        public string Vin { get; set; }
        /// <summary>
        /// Rough price, specified by the client
        /// </summary>
        [Required]
        public decimal DesiredPrice { get; set; }
    }
}
