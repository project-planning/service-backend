﻿using System.Collections.Generic;

namespace Repair.Api.Models.Output
{
    public class CarModels
    {
        public List<CarModelModel> AvailableCarModels { get; set; }
    }
}
