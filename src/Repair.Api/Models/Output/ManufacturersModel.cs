﻿using System.Collections.Generic;

namespace Repair.Api.Models.Output
{
    public class ManufacturersModel
    {
        public List<ManufacturerModel> Manufacturers { get; set; }
    }
}
