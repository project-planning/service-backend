﻿using System;

namespace Repair.Api.Models.Output
{
    public class IssueResponseModel
    {
        public long Id { get; set; }
        /// <summary>
        /// Car manufacturer company
        /// </summary>
        public ManufacturerModel Manufacturer { get; set; }
        /// <summary>
        /// Car model
        /// </summary>
        public CarModelModel Model { get; set; }
        /// <summary>
        /// The year car has been produced
        /// </summary>
        public int ProductionYear { get; set; }
        /// <summary>
        /// Issue description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Car's unique identification number
        /// </summary>
        public string Vin { get; set; }
        /// <summary>
        /// Client's unique identity
        /// </summary>
        public long ClientId { get; set; }
        /// <summary>
        /// Utc time, when issue was posted
        /// </summary>
        public DateTime Created { get; set; }
        /// <summary>
        /// Rough price, specified by the client
        /// </summary>
        public decimal DesiredPrice { get; set; }
        /// <summary>
        /// Offer, that client selected. Can be null
        /// </summary>
        public long? SelectedOfferId { get; set; }
    }
}
