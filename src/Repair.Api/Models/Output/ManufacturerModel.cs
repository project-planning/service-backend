﻿namespace Repair.Api.Models.Output
{
    public class ManufacturerModel
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
