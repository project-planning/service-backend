﻿namespace Repair.Api.Models.Output
{
    public class UserLoginResult
    {
        public UserInfoModel UserInfo { get; set; }
        public string AccessToken { get; set; }

        public AuthenticationType AuthenticationType { get; set; }
        
        
    }

    public enum AuthenticationType
    {
        LoginPass,
        Registration,
        ThirdParty
    }
}
