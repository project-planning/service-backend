namespace Repair.Api.Models.Output
{
    public class OfferResponseModel
    {
        public long Id { get; set; }

        public decimal Price { get; set; }
        
        public long IssueId { get; set; }

        public string Description { get; set; }

        public AutoServiceResponseModel AutoService { get; set; }
    }
}