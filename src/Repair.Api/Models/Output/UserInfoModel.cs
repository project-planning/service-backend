namespace Repair.Api.Models.Output
{
    public class UserInfoModel
    {
        public string UserName { get; set; }
        
        public string Email { get; set; }
    }
}