﻿using Repair.Data.Enums;

namespace Repair.Api.Models.Output
{
    // Model model lol
    public class CarModelModel
    {
        public long Id { get; set; }

        public long ManufacturerId { get; set; }

        public string ModelName { get; set; }

        public string AdditionalInfo { get; set; }

        public int DoorCount { get; set; }

        public int ProductionStartYear { get; set; }

        public int? ProductionEndYear { get; set; }

        public TransmissionType TransmissionType { get; set; }

        public FuelType FuelType { get; set; }

        public string PictureUrl { get; set; }
    }
}
