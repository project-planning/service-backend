namespace Repair.Api
{
    public class AppSettings
    {
        public string JwtSigningKey { get; set; }
        
        public long JwtTtlInSeconds { get; set; }
    }
}