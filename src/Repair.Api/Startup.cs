using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Repair.Api.Models.Input;
using Repair.Data;
using Serilog;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Converters;
using System.Reflection;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Omu.ValueInjecter;
using Repair.Data.Entities;
using Repair.Api.Models.Output;

namespace Repair.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        const string CorsPolicy = "_autoServiceCorsPolicy";


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var databaseConnection = Configuration.GetConnectionString("Database");
            if (string.IsNullOrWhiteSpace(databaseConnection))
            {
                throw new Exception("Please provide database connection string with ConnectionStrings__Database environment variable");
            }

            services.AddDbContextPool<AutoserviceContext>(opt => opt.UseMySql(databaseConnection, 
                opt => opt.EnableRetryOnFailure(10)), 10);

            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(appSettings.JwtSigningKey));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = key,
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                    };
                });

            services.AddCors(options =>
            {
                options.AddPolicy(CorsPolicy,
                builder =>
                {
                    builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                });
            });

            services.AddControllersWithViews();
            services.AddSwaggerGen(c =>
            {
                var scheme = new OpenApiSecurityScheme()
                {
                    Description =
                        "JWT Authorization header using the Bearer scheme. Just the access token is required.",
                    Scheme = "bearer",
                    Type = SecuritySchemeType.Http,
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header
                };
                

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {new OpenApiSecurityScheme 
                        {Reference = new OpenApiReference 
                            {Type = ReferenceType.SecurityScheme, Id = "bearerAuth"}},
                        new List<string>()}
                });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

                c.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
                c.OperationFilter<SecurityRequirementsOperationFilter>(true, "bearerAuth");

                c.AddSecurityDefinition("bearerAuth", scheme);
                c.ExampleFilters();
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Repair service API", Version = "v1" });

                
            });
            services.AddSwaggerExamplesFromAssemblyOf<RegisterUserModel.Example>();

            services.AddRouting(r =>
            {
                r.LowercaseUrls = true;
                r.LowercaseQueryStrings = true;
            });
            services
                .AddMvc(o =>
                {
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .Build();
                    o.Filters.Add(new AuthorizeFilter(policy));
                })
                .AddNewtonsoftJson(opt =>
                {
                    opt.SerializerSettings.Converters.Add(new StringEnumConverter());
                })
                .AddFluentValidation(fv =>
                {
                    fv.RegisterValidatorsFromAssemblyContaining<RegisterUserModel.RegisterModelValidator>();
                    fv.ImplicitlyValidateChildProperties = true;
                });
            services.AddDataProtection(x => x.ApplicationDiscriminator = "repair-api")
                .PersistKeysToDbContext<AutoserviceContext>();

            services.AddSingleton<IPasswordHasher<string>, PasswordHasher<string>>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            InitMappings();
            using var serviceScope = app.ApplicationServices.
                GetRequiredService<IServiceScopeFactory>().
                CreateScope();
            using var context = serviceScope.ServiceProvider.GetService<AutoserviceContext>();
            context.Database.Migrate();

            app.UseCors(CorsPolicy);
            app.UseSwagger();
            app.UseSwaggerUI(c => 
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Repair service API v1");
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            //app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseSerilogRequestLogging();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private void InitMappings()
        {
            Mapper.AddMap<Issue, IssueResponseModel>(issue => new IssueResponseModel
            {
                Id = issue.Id,
                ClientId = issue.ClientId,
                Created = issue.Created,
                Description = issue.Description,
                DesiredPrice = issue.DesiredPrice,
                Manufacturer = Mapper.Map<ManufacturerModel>(issue.Model.Manufacturer),
                Model = Mapper.Map<CarModelModel>(issue.Model),
                ProductionYear = issue.ProductionYear,
                SelectedOfferId = issue.SelectedOfferId,
                Vin = issue.Vin
            });
            
            Mapper.AddMap<AutoService, AutoServiceResponseModel>(service => new AutoServiceResponseModel
            {
                Address = service.Address,
                Description = service.Description,
                Id = service.Id,
                Name = service.Name,
                ContactEmail = service.ContactEmail,
                ContactPhone = service.ContactPhone
            });
            
            Mapper.AddMap<Offer, OfferResponseModel>(offer => new OfferResponseModel
            {
                Description = offer.Description,
                Id = offer.Id,
                IssueId = offer.IssueId,
                Price = offer.Price,
                AutoService = Mapper.Map<AutoServiceResponseModel>(offer.AutoService),
            });
        }
    }
}
