﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repair.Data;
using System;
using System.Collections;
using System.Collections.Generic;

namespace repair_backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TestController : ControllerBase
    {
        [HttpGet, AllowAnonymous]
        public ActionResult<IEnumerable<TestItem>> Get()
        {
            return new[] { new TestItem { PropA = "one", PropB = 1 }, new TestItem {PropA = "Two", PropB = 2 } };
        }

        [HttpGet(nameof(GetEnvironmentVariables)), Authorize(Roles = "Admin")]
        public ActionResult<IDictionary> GetEnvironmentVariables()
        {
            return new ActionResult<IDictionary>(Environment.GetEnvironmentVariables());
        }
    }
}
