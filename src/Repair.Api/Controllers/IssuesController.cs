﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Omu.ValueInjecter;
using Repair.Api.Models.Input;
using Repair.Api.Models.Output;
using Repair.Data;
using Repair.Data.Entities;
using Repair.Data.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repair.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IssuesController : RepairControllerBase
    {
        private readonly AutoserviceContext _context;

        public IssuesController(AutoserviceContext context)
        {
            _context = context;
        }

        [HttpGet, Authorize(Roles = nameof(Role.RepairShop))]
        public async Task<ActionResult<ICollection<IssueResponseModel>>> Get()
        {
            var issues = await _context.Issues
                .Include(i => i.Model)
                    .ThenInclude(m => m.Manufacturer)
                .Where(i => !i.SelectedOfferId.HasValue)
                .ToListAsync();

            return issues.Select(i => Mapper.Map<IssueResponseModel>(i)).ToList();
        }

        /// <summary>
        /// Returns all user's Issues
        /// </summary>
        /// <returns></returns>
        /// <response code="200">If everything is ok</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("my"), Authorize(Roles = nameof(Role.RegularUser))]
        public async Task<ActionResult<ICollection<IssueResponseModel>>> GetMy()
        {
            var issues = await _context.Issues
                .Include(i => i.Model)
                    .ThenInclude(m => m.Manufacturer)
                .Where(i => !i.SelectedOfferId.HasValue && i.ClientId == UserId)
                .ToListAsync();

            return issues.Select(i => Mapper.Map<IssueResponseModel>(i)).ToList();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IssueResponseModel>> Get(long id)
        {
            var issue = await _context.Issues
                .Include(i => i.Model)
                    .ThenInclude(m => m.Manufacturer)
                .Where(i => !i.SelectedOfferId.HasValue)
                .FirstOrDefaultAsync(i => i.Id == id);

            if (issue == null)
                return NotFound();

            return Mapper.Map<IssueResponseModel>(issue);
        }

        /// <summary>
        /// Creates a new issue
        /// </summary>
        /// <param name="issueSaveModel"></param>
        /// <returns></returns>
        /// <response code="201">If created successfully</response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost]
        [Authorize(Roles = nameof(Role.RegularUser))]
        public async Task<ActionResult> Create(IssueSaveModel issueSaveModel)
        {
            var newIssue = Mapper.Map<Issue>(issueSaveModel);
            newIssue.ClientId = UserId;
            _context.Issues.Add(newIssue);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(Get), new { id =  newIssue.Id}, null);
        }
    }
}
