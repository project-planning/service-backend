﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Repair.Api.Models.Input;
using Repair.Api.Models.Output;
using Repair.Data;
using Repair.Data.Entities;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Omu.ValueInjecter;
using Repair.Data.Enums;
using Serilog;

namespace Repair.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : RepairControllerBase
    {
        private readonly AutoserviceContext _context;
        private readonly IPasswordHasher<string> _passwordHasher;
        private readonly IOptions<AppSettings> _appSettings;

        public AuthController(AutoserviceContext context, 
            IPasswordHasher<string> passwordHasher,
            IOptions<AppSettings> appSettings)
        {
            _context = context;
            _passwordHasher = passwordHasher;
            _appSettings = appSettings;
        }

        [HttpPost(nameof(Register)), AllowAnonymous]
        public async Task<ActionResult<UserLoginResult>> Register(RegisterUserModel registerModel)
        {
            var u = await _context.Users.SingleOrDefaultAsync(
                user => user.Email == registerModel.Email ||
                user.UserName == registerModel.UserName);

            if (u != null)
            {
                if (u.UserName == registerModel.UserName)
                {
                    ModelState.AddModelError(nameof(RegisterUserModel.UserName), "Username already exists");
                    return UnprocessableEntity(ModelState);
                }

                if (u.Email == registerModel.Email)
                {
                    ModelState.AddModelError(nameof(RegisterUserModel.Email), "Email already exists");
                    return UnprocessableEntity(ModelState);
                } 
            }

            var passHash = _passwordHasher.HashPassword(registerModel.UserName, registerModel.Password);
            var user = new User
            {
                Email = registerModel.Email,
                UserName = registerModel.UserName, 
                PasswordHash = passHash, 
                Roles = new List<UserRole>{ new UserRole{Role = Role.RegularUser}}
            };

            await _context.AddAsync(user);
            await _context.SaveChangesAsync();

            var userInfo = (UserInfoModel)new UserInfoModel().InjectFrom(user);
            return new UserLoginResult
            {
                AuthenticationType = AuthenticationType.LoginPass,
                AccessToken = GenerateJwt(user, AuthenticationType.LoginPass),
                UserInfo = userInfo
            };
        }

        [HttpPost(nameof(Login)), AllowAnonymous]
        public async Task<ActionResult<UserLoginResult>> Login(AuthenticateModel authenticateModel)
        {
            var user = await _context.Users
                .Include(u => u.Roles)
                .FirstOrDefaultAsync(u => u.UserName == authenticateModel.Login ||
                                               u.Email == authenticateModel.Login);

            if (user == null)
                return NotFound();

            var validationResult = _passwordHasher.VerifyHashedPassword(user.UserName,
                user.PasswordHash, authenticateModel.Password);

            if (validationResult == PasswordVerificationResult.Failed)
                return Forbid();

            if (validationResult == PasswordVerificationResult.SuccessRehashNeeded)
            {
                user.PasswordHash = _passwordHasher.HashPassword(user.UserName, authenticateModel.Password);

                await _context.SaveChangesAsync();
            }
            
            var userInfo = (UserInfoModel)new UserInfoModel().InjectFrom(user);
            return new UserLoginResult
            {
                AuthenticationType = AuthenticationType.LoginPass,
                AccessToken = GenerateJwt(user, AuthenticationType.LoginPass),
                UserInfo = userInfo
            };
        }

        private string GenerateJwt(User user, AuthenticationType authenticationType)
        {
            var userRoles = user.Roles
                .Select(role => new Claim(ClaimTypes.Role ,role.Role.ToString()));
            
            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Value.JwtSigningKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.AuthenticationMethod, authenticationType.ToString()),
                }.Concat(userRoles)),
                Expires = DateTime.UtcNow.AddSeconds(_appSettings.Value.JwtTtlInSeconds),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
