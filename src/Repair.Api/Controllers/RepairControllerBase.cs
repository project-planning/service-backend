﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;

namespace Repair.Api.Controllers
{
    public class RepairControllerBase : ControllerBase
    {
        public long UserId => Convert.ToInt64(User.Identities.First().Claims.First(cl => cl.Type == ClaimTypes.Name).Value);
    }
}
