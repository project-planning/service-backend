﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Omu.ValueInjecter;
using Repair.Api.Models.Output;
using Repair.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Repair.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ManufacturersController : RepairControllerBase
    {
        private readonly AutoserviceContext _context;

        public ManufacturersController(AutoserviceContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<ManufacturersModel>> GetAsync()
        {
            var manufacturers = await _context.Manufacturers.ToListAsync();

            return new ManufacturersModel
            {
                Manufacturers = manufacturers
                .Select(m => new ManufacturerModel { Id = m.Id, Name = m.Name })
                .ToList()
            };
        }

        [HttpGet("{manufacturerId:long}/models")]
        public async Task<ActionResult<CarModels>> GetCarModelsByManufacturer(long manufacturerId)
        {
            var manufacturer = await _context.Manufacturers
                .Include(m => m.Models)
                .FirstOrDefaultAsync(m => m.Id == manufacturerId);

            if (manufacturer == null)
                return NotFound();

            var models = manufacturer.Models.ToList();
            return new CarModels
            {
                AvailableCarModels = models
                .Select(m => Mapper.Map<CarModelModel>(m))
                .ToList()
            };
        }
    }
}
