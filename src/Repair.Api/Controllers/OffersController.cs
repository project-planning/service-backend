﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repair.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Omu.ValueInjecter;
using Repair.Api.Models.Input;
using Repair.Api.Models.Output;
using Repair.Data;
using Repair.Data.Entities;

namespace Repair.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OffersController : RepairControllerBase
    {
        private readonly AutoserviceContext _context;

        public OffersController(AutoserviceContext context)
        {
            _context = context;
        }
        
        [HttpGet("api/issues/{issue_id:long}/[controller]")]
        public async Task<ActionResult<List<OfferResponseModel>>> GetOffers(long issue_id)
        {
            return (await _context.Offers.Include(o => o.AutoService)
                    .Where(o => o.IssueId == issue_id)
                .ToListAsync())
                .Select(o => Mapper.Map<OfferResponseModel>(o))
                .ToList();
        }

        /// <summary>
        /// Returns all repair shop's offers
        /// </summary>
        /// <returns></returns>
        /// <response code="200">If everything is ok</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("my"), Authorize(Roles = nameof(Role.RepairShop))]
        public async Task<ActionResult<ICollection<OfferResponseModel>>> GetMy()
        {
            return (await _context.Offers.Include(o => o.AutoService)
                    .Where(o => o.AutoService.User.Id == UserId)
                    .ToListAsync())
                .Select(o => Mapper.Map<OfferResponseModel>(o))
                .ToList();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<OfferResponseModel>> Get(long id)
        {
            return Mapper.Map<OfferResponseModel>(
                await _context.Offers
                    .Include(o => o.AutoService)
                    .FirstOrDefaultAsync(o => o.Id == id));
        }

        /// <summary>
        /// Creates a new offer
        /// </summary>
        /// <param name="offerSaveModel"></param>
        /// <returns></returns>
        /// <response code="201">If created successfully</response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost("api/issues/{issue_id:long}/[controller]")]
        [Authorize(Roles = nameof(Role.RepairShop))]
        public async Task<ActionResult> Create(OfferSaveModel offerSaveModel)
        {
            var autoService = await _context.AutoServices.FirstOrDefaultAsync(s => s.UserId == UserId);
            
            if (autoService == null)
                return new BadRequestObjectResult("User is not registered as auto service");
            
            var offer = new Offer
            {
                Description = offerSaveModel.Description,
                IssueId = offerSaveModel.IssueId,
                Price = offerSaveModel.Price,
                AutoService = autoService
            };

            _context.Offers.Add(offer);
            await _context.SaveChangesAsync();
            
            return CreatedAtAction(nameof(Get), new { id = offer.Id }, offerSaveModel);
        }
    }
}
