# service-backend
The application requires you to have a MariaDB instance running.
You must pass a connection string either via `ConnectionStrings__Database` environment variable, or with user secrets/config files.


### Docker
Keep in mind, that localhost in docker container doesn't match the localhost on your host.
a nice way to access it is via this: `host.docker.internal`

Running application
```bash 
docker build -t autoservice .
docker run -d -e ConnectionStrings__Database="server=<host>;Database=<database>;Uid=<user>;Pwd=<password>;" -p 80:80 autoservice
```

