FROM mcr.microsoft.com/dotnet/core/aspnet:3.0-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.0-buster AS build
WORKDIR /src
COPY ["src/Repair.Api/Repair.Api.csproj", "Repair.Api/"]
COPY ["src/Repair.Data/Repair.Data.csproj", "Repair.Data/"]
RUN dotnet restore "./Repair.Api/Repair.Api.csproj"
COPY src/ .
WORKDIR "/src/Repair.Api"
RUN dotnet build "Repair.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "/src/Repair.Api/Repair.Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Repair.Api.dll"]